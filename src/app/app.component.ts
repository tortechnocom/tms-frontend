import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, MenuController, AlertController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { HomePage } from '../pages/home/home';
import { ProfilePage } from '../pages/profile/profile';
import { LoginPage } from '../pages/login/login';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { AdminPage } from '../pages/admin/admin';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: "tms-app",
  templateUrl: 'app.html'
})
export class TMSApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any;
  public static language: any;
  languageList = [
    {language: "th", name: "Thai", selected: "selected"},
    {language: "au", name : "English", selected: ""}]
    // APIs
  public static apiUrl:string = "https://tms-backend.mymemoapps.com/api/";
  public static authToken:string;
  public static pages: Array<{id: string,title: string, component: any, visible: boolean, icon: string}>;
  public static role = "CUSTOMER";
  public static uiLabelMap: any;

  constructor(public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public menu: MenuController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
  ) {
    this.setLanguage(localStorage.getItem("language"));
    this.getUiLabels();
  }

  initializeApp() {
    TMSApp.pages = [
      { id: "GLOBAL", title: TMSApp.uiLabelMap.APIGlobal, component: HomePage, visible: true, icon: "globe" },
      { id: "DASHBOARD", title: TMSApp.uiLabelMap.APIDashboard, component: DashboardPage, visible: false, icon: "apps"},
      { id: "ADMIN", title: TMSApp.uiLabelMap.APIAdmin, component: AdminPage, visible: false, icon: "construct"},
      { id: "ACCOUNT", title: TMSApp.uiLabelMap.APIAccount, component: ProfilePage, visible: false, icon: "person"},
      { id: "SIGN_IN", title: TMSApp.uiLabelMap.APISignIn, component: LoginPage, visible: true, icon: "lock"}
    ];
    TMSApp.setUser(null, null);
    TMSApp.refreshMenu();

    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }
  setLanguage(value) {
    if (value ==  null) value = "au";
    if (value == "au") {
      TMSApp.language = "en";
    } else {
      TMSApp.language = value
    }
    this.getUiLabels();
    localStorage.setItem("language", value);
    let count = 0;
    for(let lang  of this.languageList) {
      if (lang.language == value) {
        this.languageList[count].selected = "selected";
      } else {
        this.languageList[count].selected = "";
      }
      count++;
    }
    if (this.nav) {
      if ("HomePage" == this.nav.root.name) {
        this.nav.goToRoot({});
      }
    }
  }
  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    //this.rootPage = page.component;
    this.nav.root = page.component
    this.menu.close();
  }
  public static refreshMenu() {
    var page;
    if (TMSApp.authToken == null) { // Logged out
      for (page of TMSApp.pages) {
        if (page.id == "SIGN_IN" || page.id == "GLOBAL") {
          page.visible = true;
        } else {
          page.visible = false;
        }
      }
    } else { // Logged in
      for (page of this.pages) {
        if (page.id == "SIGN_IN") {
          page.visible = false;
        } else {
          if (page.id == "ADMIN") {
            if (TMSApp.role == 'ADMIN') {
              page.visible = true;
            } else {
              page.visible = false;
            }
          } else {
            page.visible = true;
          }
          
        }
      }
    }
  }
  static setUser(authToken, role) {
    if (authToken == null && sessionStorage.getItem("authToken") != null) {
      authToken = sessionStorage.getItem("authToken");
      role = sessionStorage.getItem("role");
    } else if (authToken != null) {
      sessionStorage.setItem('authToken', authToken);
      sessionStorage.setItem('role', role);
    }
    TMSApp.authToken = authToken;
    TMSApp.role = role
  }
  static getAuthToken(authToken) {
        TMSApp.authToken;
  }
  static clearAuthToken () {
    TMSApp.authToken = null;
    sessionStorage.removeItem('authToken');
  }
  static getPages () {
    return this.pages;
  }
  get pages() {
    return TMSApp.pages;
  }
  get uiLabelMap () {
    return TMSApp.uiLabelMap;
  }
  getUiLabels() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(TMSApp.apiUrl + 'getUiLabels', {language: TMSApp.language}, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        TMSApp.uiLabelMap = res["uiLabelMap"];
        this.initializeApp();
        this.rootPage = HomePage;
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
}
