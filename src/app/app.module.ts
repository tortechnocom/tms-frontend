import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { TMSApp } from './app.component';
import { HomePage } from '../pages/home/home';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPage } from '../pages/login/login';
import { ProfilePage } from '../pages/profile/profile';
import { RegisterPage } from '../pages/register/register';
import { RegisterStartPage } from '../pages/register-start/register-start';
import { HttpClientModule } from '@angular/common/http';
import { ForgotPage } from '../pages/forgot/forgot';
import { ForgotSetPage } from '../pages/forgotset/forgot.set';
import { DashboardPage } from '../pages/dashboard/dashboard';
import { DashboardDeviceNewPage, DashboardDeviceQrPage } from '../pages/dashboard-device-new/dashboard-device-new';
import { DashboardDeviceViewPage } from '../pages/dashboard-device-view/dashboard-device-view';
import { AdminDeviceViewPage } from '../pages/admin-device-view/admin-device-view';
import { AdminPage } from '../pages/admin/admin';
import { NewsPage } from '../pages/news/news';
import { QRScanner } from '@ionic-native/qr-scanner';
import { DeviceSetting, DeviceView, DeviceNotification } from '../pages/dashboard-device-view-tabs/dashboard-device-view-tabs';
import { GoogleMaps } from '@ionic-native/google-maps';

@NgModule({
  declarations: [
    TMSApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage,
    DashboardPage,
    DashboardDeviceNewPage, DashboardDeviceViewPage, DashboardDeviceQrPage,
    AdminDeviceViewPage,
    AdminPage,
    DeviceSetting, DeviceView, DeviceNotification,
    NewsPage
    
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(TMSApp),
    HttpClientModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    TMSApp,
    HomePage,
    LoginPage,
    ProfilePage,
    RegisterPage,
    ForgotPage,
    ForgotSetPage,
    RegisterStartPage,
    DashboardPage,
    DashboardDeviceNewPage, DashboardDeviceViewPage, DashboardDeviceQrPage,
    AdminDeviceViewPage,
    AdminPage,
    DeviceSetting, DeviceView, DeviceNotification,
    NewsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    QRScanner,
    GoogleMaps
  ]
})
export class AppModule {}
