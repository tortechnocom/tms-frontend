import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController, LoadingController} from 'ionic-angular';
import { ForgotSetPage } from '../forgotset/forgot.set';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'forgot-page',
  templateUrl: 'forgot.html'
})
export class ForgotPage {
  private input: any = {
    email: null
  };
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient,
    public loadingCtrl: LoadingController
    ) {
  }
  sendCodeEmail() {
    let loader = this.loadingCtrl.create({
      content: "Sending..."
    });
    loader.present();
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(TMSApp.apiUrl + 'forgotPassword', JSON.stringify(this.input), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      loader.dismiss();
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['successMessage'],
          buttons: ['OK']
        });
        alert.present();
        this.nav.push(ForgotSetPage, {email: this.input.email});
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
