import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController} from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'news-page',
  templateUrl: 'news.html'
})
export class NewsPage {
  news: any;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
    ) {
      this.getNews();
  }
  getNews() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'getNews', null, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.news = res["news"]
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
