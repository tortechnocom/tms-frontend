import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController, NavParams} from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DeviceSetting, DeviceView, DeviceNotification } from '../dashboard-device-view-tabs/dashboard-device-view-tabs';


@Component({
  selector: 'dashboard-device-view-page',
  templateUrl: 'dashboard-device-view.html'
})
export class DashboardDeviceViewPage {
  public static deviceId: string;
  public static deviceName: string;
  public static deviceQrUrl: string;
  public static callback: any;
  tabView: any;
  tabNotification: any;
  tabSetting: any;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
    ) {
      DashboardDeviceViewPage.callback    = this.navParams.get('callback');
      this.tabView = DeviceView;
      this.tabNotification = DeviceNotification;
      this.tabSetting = DeviceSetting;
      DashboardDeviceViewPage.deviceId = this.navParams.get("deviceId");
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': TMSApp.authToken
        };
      this.httpClient.post(TMSApp.apiUrl + 'getDevice',
        JSON.stringify({deviceId: DashboardDeviceViewPage.deviceId}), {
          headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          DashboardDeviceViewPage.deviceId = res["deviceId"];
          DashboardDeviceViewPage.deviceName = res["deviceName"];
          DashboardDeviceViewPage.deviceQrUrl = res["deviceQrUrl"];
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  get deviceName() {
    return DashboardDeviceViewPage.deviceName;
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
