import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController} from 'ionic-angular';
import { AdminDeviceViewPage } from '../admin-device-view/admin-device-view';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'admin-page',
  templateUrl: 'admin.html'
})
export class AdminPage {
  deviceList: Array<{}>;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
    ) {
      this.getDeviceList();
  }
  createDevice() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'createDevice', null, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.getDeviceList();
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  goToDevice(deviceId) {
    this.nav.push(AdminDeviceViewPage, {deviceId: deviceId});
  }
  getDeviceList() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'getDeviceList', null, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.deviceList = res["deviceList"]
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
