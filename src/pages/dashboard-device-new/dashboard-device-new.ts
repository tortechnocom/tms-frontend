import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import { NavController, AlertController, NavParams } from 'ionic-angular';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { QRScanner, QRScannerStatus } from '@ionic-native/qr-scanner';

@Component({
  selector: 'dashboard-device-new-page',
  templateUrl: 'dashboard-device-new.html'
})
export class DashboardDeviceNewPage {
  private input: any = {
    deviceId: null,
    deviceName: null
  };
  callback: any;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    private httpClient: HttpClient,
    public navParams: NavParams
    ) {
      this.callback    = this.navParams.get('callback');
  }
  scanQr() {
    var getQrCode = (deviceId) => {
      return new Promise((resolve, reject) => {
        console.log(deviceId);
        this.input.deviceId = deviceId;
         resolve();
      });
    }
    this.nav.push(DashboardDeviceQrPage, {
      deviceId: this.input.deviceId,
      callback: getQrCode
    });
  }
  addDevice() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'addDevice', JSON.stringify(this.input), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.callback().then(()=>{ this.nav.pop() });
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}

@Component({
  selector: 'dashboard-device-qr',
  templateUrl: 'dashboard-device-qr.html'
})
export class DashboardDeviceQrPage {
  callback: any;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    private qrScanner: QRScanner,
    public navParams: NavParams
  ) {
    this.callback    = this.navParams.get('callback');
      //this.qrScanner.useBackCamera();
    this.qrScanner.prepare()
    .then((status: QRScannerStatus) => {
      if (status.authorized) {
        // camera permission was granted
        console.log("authorized.....");
        // start scanning
        let scanSub = this.qrScanner.scan().subscribe((deviceId: string) => {
          console.log(deviceId);
          this.qrScanner.hide(); // hide camera preview
          scanSub.unsubscribe(); // stop scanning
          
          this.callback(deviceId).then(()=>{ this.nav.pop() });
        });
        // show camera preview
        this.qrScanner.show()
        .then((data : QRScannerStatus)=> {
          console.log(data.showing);
          if (data.showing == false) {
            this.qrScanner.resumePreview();
          }
        },err => {
          console.log("err...");
        });
        // wait for user to scan something, then the observable callback will be called

      } else if (status.denied) {
        // camera permission was permanently denied
        // you must use QRScanner.openSettings() method to guide the user to the settings page
        // then they can grant the permission from there
        console.log("camera permission was permanently denied.");
      } else {
        // permission was denied, but not permanently. You can ask for permission again at a later time.
        console.log("permission was denied, but not permanently.");
      }
    })
    .catch((e: any) => console.log('Error is', e));
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}