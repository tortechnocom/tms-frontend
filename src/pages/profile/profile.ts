import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AlertController, NavController } from 'ionic-angular';
import { HomePage } from '../home/home';

@Component({
  selector: 'profile-page',
  templateUrl: 'profile.html'
})

export class ProfilePage {
    email: string;
    showPassword = false;
    constructor(
        public httpClient: HttpClient,
        public alertCtrl: AlertController,
        public nav: NavController,
    ) {
        this.getAccount();
    }
    getAccount() {
        let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': TMSApp.authToken
            };
          this.httpClient.post(TMSApp.apiUrl + 'getAccount', null, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              this.email = res['email']
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
    }
    signOut() {
        return new Promise((resolve, reject) => {
          let headerJson = {
            'Content-Type': 'application/json',
            'Accept': 'application/json',
            'Authorization': TMSApp.authToken
            };
          this.httpClient.post(TMSApp.apiUrl + 'logout', null, {
            headers: new HttpHeaders(headerJson)
          })
          .subscribe(res => {
            let title = "Response";
            if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
              let alert = this.alertCtrl.create({
                title: title,
                subTitle: res['errorMessage'],
                buttons: ['OK']
              });
              alert.present();
            } else {
              TMSApp.clearAuthToken();
              TMSApp.refreshMenu();
              this.nav.setRoot(HomePage); 
              this.nav.popToRoot();
            }
            //resolve(res);
          }, (err) => {
            console.log('err: ', err);
            //reject(err);
          });
        });
      }
      get uiLabelMap() {
        return TMSApp.uiLabelMap;
      }
    
}