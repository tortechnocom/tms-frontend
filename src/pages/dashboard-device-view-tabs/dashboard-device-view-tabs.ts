import { Component, ViewChild } from '@angular/core';
import { TMSApp } from '../../app/app.component';
import { NavController, AlertController, NavParams, App, ToastController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Chart } from 'chart.js';
import { DashboardDeviceViewPage} from '../dashboard-device-view/dashboard-device-view';

@Component({
  selector: 'dashboard-device-view-tab-view',
  templateUrl: 'dashboard-device-view-tab-view.html'
})
export class DeviceView {
  @ViewChild('lineCanvas') lineCanvas;
  times: any;
  values: any;
  lineChart: any;
  duration = "24h";
  min = 0;
  max = 100;
  temperature = {lower: 0, upper: 100};
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
    ) {
    }
  get deviceId () {
    return DashboardDeviceViewPage.deviceId;
  }
  get deviceName () {
    return DashboardDeviceViewPage.deviceName;
  }
  get deviceQrUrl () {
    return DashboardDeviceViewPage.deviceQrUrl;
  }
  ionViewDidLoad() {
    //----- Get Temperature
      this.getTemperatureList();
  }
  updateTemperatureList() {
    this.getTemperatureList();
    this.lineChart.data.labels.push(this.times);
    this.lineChart.data.datasets.forEach((dataset) => {
        dataset.data.push(this.values);
    });
    this.lineChart.update();
  }
  getTemperatureList() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'getDeviceTemperatureList', JSON.stringify({
        deviceId: DashboardDeviceViewPage.deviceId,
        duration: this.duration
    }), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        let timeStrs: Array<string> = res["times"];
        let times = new Array<Date>();
        if (timeStrs != null) {
          timeStrs.forEach(element => {
            let date = new Date(element + "");
            times.push(date);
          });
        }
        this.times = times;
        this.values = res["values"];
        this.min = res["min"];
        this.max = res["max"];
        this.createChart();
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  createChart() {
    let data = {
      labels: this.times,
      datasets: [
          {
              label: "Temperature",
              fill: false,
              lineTension: 10,
              backgroundColor: "rgba(75,192,192,0.4)",
              borderColor: "rgba(75,192,192,1)",
              borderCapStyle: 'butt',
              borderDash: [],
              borderDashOffset: 0,
              borderJoinStyle: 'miter',
              pointBorderColor: "rgba(75,192,192,1)",
              pointBackgroundColor: "#fff",
              pointBorderWidth: 1,
              pointHoverRadius: 5,
              pointHoverBackgroundColor: "rgba(75,192,192,1)",
              pointHoverBorderColor: "rgba(220,220,220,1)",
              pointHoverBorderWidth: 1,
              pointRadius: 0,
              pointHitRadius: 10,
              data: this.values,
              spanGaps: false,
              cubicInterpolationMode: 'monotone'
          }
        ]
    };
    let options = {
      type: 'line',
      data: data,
      options: {
        fill: false,
        responsive: true,
        scales: {
          xAxes: [{
              type: 'time',
              time: {
                displayFormats: {
                  hour: 'MMM D, hA'
                },
                distribution: 'series'
              },
              display: true,
              scaleLabel: {
                display: true,
                labelString: "Date",
              }
          }],
          yAxes: [{
            ticks: {
              beginAtZero: true,
            },
            display: true,
            scaleLabel: {
              display: true,
              labelString: "°C",
            }
          }]
        }
      }
    }
    this.lineChart = new Chart(this.lineCanvas.nativeElement, options);
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
@Component({
  selector: 'dashboard-device-view-tab-notification',
  templateUrl: 'dashboard-device-view-tab-notification.html'
})
export class DeviceNotification {
  temperature:  any;
  checked: boolean;
  status: string;
  repeat = 10;
  repeatList = [
    {id: "R05", value: 5, active: false},
    {id: "R10", value: 10, active: true},
    {id: "R30", value: 30, active: false},
    {id: "R60", value: 60, active: false}
  ];
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
  ) {
    this.temperature = { lower: 0, upper: 100 };
    this.checked = false;
    this.status = "OFF";
    this.getNotification();
  }
  setRepeat(repeat) {
    let count = 0;
    for (let repeatCheck of this.repeatList) {
      if (repeat.id == repeatCheck.id) {
        this.repeatList[count].active = true;
      } else {
        this.repeatList[count].active = false;
      }
      count++;
    }
    this.repeat = repeat.value;
    this.updateNotification();
  }
  getNotification(){
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'getDeviceNotification', JSON.stringify({deviceId: DashboardDeviceViewPage.deviceId}), {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.temperature = { lower: res["min"], upper: res["max"] };
        this.checked = res["status"] == "OFF" ? false : true;
        this.status = res["status"];
        this.repeat = res["repeat"];
        let count = 0;
        for (let repeatCheck of this.repeatList) {
          if (this.repeat == repeatCheck.value) {
            this.repeatList[count].active = true;
          } else {
            this.repeatList[count].active = false;
          }
          count++;
        }
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  updateNotification() {
    if (this.checked == true) {
      this.status = "ON";
    } else {
      this.status = "OFF";
    }
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
      let data = JSON.stringify({
        deviceId: DashboardDeviceViewPage.deviceId,
        status: this.status,
        max: this.temperature["upper"],
        min: this.temperature["lower"],
        repeat: this.repeat
      });
    this.httpClient.post(TMSApp.apiUrl + 'updateDeviceNotification', data, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
@Component({
  selector: 'dashboard-device-view-tab-setting',
  templateUrl: 'dashboard-device-view-tab-setting.html'
})
export class DeviceSetting {
  private input: any =  {
    deviceName: null
  };
  private hide = true;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    private httpClient: HttpClient,
    public navParams: NavParams,
    public app: App,
    public toastCtrl: ToastController
    ) {
      this.input.deviceName = DashboardDeviceViewPage.deviceName;
  }
  setHide() {
    this.hide = false;
    this.input.deviceName = DashboardDeviceViewPage.deviceName;
  }
  updateDevice() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
      let data = JSON.stringify({
        deviceId: DashboardDeviceViewPage.deviceId,
        deviceName: this.input.deviceName
      });
    this.httpClient.post(TMSApp.apiUrl + 'updateDevice', data, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        DashboardDeviceViewPage.deviceName = this.input.deviceName;
        DashboardDeviceViewPage.callback().then(()=>{ this.hide = true; });
        
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get deviceId () {
    return DashboardDeviceViewPage.deviceId;
  }
  get deviceName () {
    return DashboardDeviceViewPage.deviceName;
  }
  setDeviceName () {
    DashboardDeviceViewPage.deviceName = this.deviceName;
  }
  get deviceQrUrl () {
    return DashboardDeviceViewPage.deviceQrUrl;
  }
  removeDevice() {
    let prompt = this.alertCtrl.create({
      title: 'Warning!',
      message: "Do you want to remove device [ " + DashboardDeviceViewPage.deviceName +" ] ?",
      inputs: null,
      buttons: [
        {
          text: 'No',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: data => {
            this.doRemove();
          }
        }
      ]
    });
    prompt.present();
  }
  doRemove () {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'removeDevice',
      JSON.stringify({deviceId: DashboardDeviceViewPage.deviceId}), {
        headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        DashboardDeviceViewPage.callback().then(()=>{ this.app.getRootNav().pop() });
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}