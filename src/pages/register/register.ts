import { Component } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController, LoadingController} from 'ionic-angular';
import {RegisterStartPage} from '../../pages/register-start/register-start';


@Component({
  selector: 'register-page',
  templateUrl: 'register.html'
})

export class RegisterPage {
  private input: any = {
    email: ""
  };
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient,
    public loadingCtrl: LoadingController
    ) {

  }
  register() {
    return new Promise((resolve, reject) => {
      let loader = this.loadingCtrl.create({
        content: "Sending..."
      });
      loader.present();
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        };
      this.httpClient.post(TMSApp.apiUrl + 'sendRegisterCode', JSON.stringify(this.input), {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        loader.dismiss();
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['successMessage'],
            buttons: ['OK']
          });
          alert.present();
          this.nav.push(RegisterStartPage, {email: this.input.email});
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
