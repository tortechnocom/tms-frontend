import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController, NavParams} from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Component({
  selector: 'admin-device-view-page',
  templateUrl: 'admin-device-view.html'
})
export class AdminDeviceViewPage {
  deviceId: string;
  deviceQrUrl: string;
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public navParams: NavParams,
    public httpClient: HttpClient
    ) {
      this.deviceId = this.navParams.get("deviceId");
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'Authorization': TMSApp.authToken
        };
      console.log('this.deviceId: ' + this.deviceId);
      this.httpClient.post(TMSApp.apiUrl + 'getDevice', JSON.stringify({deviceId: this.deviceId}), {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          this.deviceId = res["deviceId"];
          this.deviceQrUrl = res["deviceQrUrl"];
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }

}
