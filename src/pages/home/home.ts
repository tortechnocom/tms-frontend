import { Component, ViewChild } from '@angular/core';
import { NavController, AlertController } from 'ionic-angular';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { TMSApp } from '../../app/app.component';
import { Chart } from 'chart.js';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  GoogleMapOptions
 } from '@ionic-native/google-maps';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  @ViewChild('doughnutChart') doughnutChart;
  availableDevice: any;
  activeDevice: any;
  allDevice: any;
  newsList: any;
  map: GoogleMap;
  constructor(
    public navCtrl: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
  ) {
    this.newsList = new Array();
  }
  ionViewDidLoad() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json'
      };
    this.httpClient.post(TMSApp.apiUrl + 'getGlobal', {language: TMSApp.language}, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.activeDevice = res["activeDevice"];
        this.availableDevice = res["availableDevice"];
        this.allDevice = this.activeDevice + this.availableDevice;
        this.newsList = res["newsList"];
        this.createChart();
        this.loadMap();
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }
  createChart() {
    let data = {
      datasets: [{
          data: [this.activeDevice, this.availableDevice],
          backgroundColor: ["green", "blue"]
      }],
  
      // These labels appear in the legend and in the tooltips when hovering different arcs
      labels: [
          'Active',
          'Available'
      ]
  };
  
    new Chart(this.doughnutChart.nativeElement, {
      type: 'doughnut',
      data: data,
      options: {
        layout: {
          padding: {
              left: 0,
              right: 0,
              top: 0,
              bottom: 0
          }
      },
        title: {
          display: true,
          text: 'All ' + this.allDevice,
          position: "bottom"
      },
        legend: {
            display: false
        }
      }
  });

  }
  loadMap() {

    let mapOptions: GoogleMapOptions = {
      camera: {
        target: {
          lat: 43.0741904,
          lng: -89.3809802
        },
        zoom: 18,
        tilt: 30
      }
    };

    this.map = GoogleMaps.create('googleMap', mapOptions);

    // Wait the MAP_READY before using any methods.
    this.map.one(GoogleMapsEvent.MAP_READY)
      .then(() => {
        console.log('Map is ready!');

        // Now you can use all methods safely.
        this.map.addMarker({
            title: 'Ionic',
            icon: 'blue',
            animation: 'DROP',
            position: {
              lat: 43.0741904,
              lng: -89.3809802
            }
          })
          .then(marker => {
            marker.on(GoogleMapsEvent.MARKER_CLICK)
              .subscribe(() => {
                alert('clicked');
              });
          });

      });
  }
}