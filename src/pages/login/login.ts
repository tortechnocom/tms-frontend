import { Component } from '@angular/core';
import { TMSApp } from '../../app/app.component';
import { NavController, AlertController } from 'ionic-angular';
import { RegisterPage } from '../register/register';
import { ForgotPage } from '../forgot/forgot';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DashboardPage } from '../dashboard/dashboard';


@Component({
  selector: 'login-page',
  templateUrl: 'login.html'
})
export class LoginPage {
  private input: any = {
    email: "",
    password: ""
  };
  private rememberMe = false;
  constructor(
    public nav: NavController,
    public httpClient: HttpClient,
    public alertCtrl: AlertController
    ) {
      if (localStorage.getItem("rememberMe")) {
        this.rememberMe = (localStorage.getItem("rememberMe") == 'true');
      }
      if (this.rememberMe == true) {
        this.input = {
          email: localStorage.getItem("email"),
          password: localStorage.getItem("password")
        }
      }
      

  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }
  login() {
    return new Promise((resolve, reject) => {
      let headerJson = {
        'Content-Type': 'application/json',
        'Accept': 'application/json'
        };
      this.httpClient.post(TMSApp.apiUrl + 'login', JSON.stringify(this.input), {
        headers: new HttpHeaders(headerJson)
      })
      .subscribe(res => {
        let title = "Response";
        if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
          let alert = this.alertCtrl.create({
            title: title,
            subTitle: res['errorMessage'],
            buttons: ['OK']
          });
          alert.present();
        } else {
          TMSApp.setUser(res['authToken'], res['role']);
          if (this.rememberMe == true) {
            localStorage.setItem('rememberMe', 'true');
            localStorage.setItem('email', this.input['email']);
          } else {
            localStorage.removeItem('rememberMe');
            localStorage.removeItem('email');
          }
          TMSApp.refreshMenu();
          this.nav.setRoot(DashboardPage);
          this.nav.popToRoot();
        }
        //resolve(res);
      }, (err) => {
        console.log('err: ', err);
        //reject(err);
      });
    });
  }
  goToRegister() {
    this.nav.push(RegisterPage);
  }
  goToForgot() {
    this.nav.push(ForgotPage);
  }
}
