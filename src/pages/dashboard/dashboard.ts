import { Component } from '@angular/core';
import {TMSApp} from '../../app/app.component';
import {NavController, AlertController} from 'ionic-angular';
import { DashboardDeviceNewPage } from '../dashboard-device-new/dashboard-device-new';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { DashboardDeviceViewPage } from '../dashboard-device-view/dashboard-device-view';


@Component({
  selector: 'dashboard-page',
  templateUrl: 'dashboard.html'
})
export class DashboardPage {
  deviceList: Array<{}>;
  
  constructor(
    public nav: NavController,
    public alertCtrl: AlertController,
    public httpClient: HttpClient
    ) {
      this.getDeviceList();
  }
  
  goToNewDevice() {
    var reloadDeviceList = () => {
      return new Promise((resolve, reject) => {
        this.getDeviceList();
         resolve();
      });
    }
    this.nav.push(DashboardDeviceNewPage, {callback: reloadDeviceList});
  }
  goToDevice(deviceId) {
    var reloadDeviceList = () => {
      return new Promise((resolve, reject) => {
        this.getDeviceList();
         resolve();
      });
    }
    this.nav.push(DashboardDeviceViewPage, {deviceId: deviceId, callback: reloadDeviceList});
  }
  getDeviceList() {
    let headerJson = {
      'Content-Type': 'application/json',
      'Accept': 'application/json',
      'Authorization': TMSApp.authToken
      };
    this.httpClient.post(TMSApp.apiUrl + 'getCustomerDeviceList', null, {
      headers: new HttpHeaders(headerJson)
    })
    .subscribe(res => {
      let title = "Response";
      if (res['responseMessage'] == "error" || res['errorMessage'] != null) {
        let alert = this.alertCtrl.create({
          title: title,
          subTitle: res['errorMessage'],
          buttons: ['OK']
        });
        alert.present();
      } else {
        this.deviceList = res["deviceList"]
      }
      //resolve(res);
    }, (err) => {
      console.log('err: ', err);
      //reject(err);
    });
  }
  get uiLabelMap() {
    return TMSApp.uiLabelMap;
  }
}
