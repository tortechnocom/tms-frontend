module.exports = {
  copyFlagIconCssFlags: {
    src: ['{{ROOT}}/node_modules/flag-icon-css/flags/**/*'],
    dest: '{{WWW}}/assets/flag-icon-css/flags'
  },
  copyFlagIconCssSass: {
    src: ['{{ROOT}}/node_modules/flag-icon-css/css/*'],
    dest: '{{WWW}}/assets/flag-icon-css/css'
  }
}